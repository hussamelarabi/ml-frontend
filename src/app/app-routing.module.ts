import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from './components/home-page/home-page.component';
import {AppComponent} from './app.component';
import {PredictorComponent} from './components/predictor/predictor.component';
import {ResultComponent} from './components/result/result.component';

const routes: Routes = [
  {
    path: 'predictor',
    component: PredictorComponent
  },
  {
    path: 'home',
    component: HomePageComponent
  },
  {
    path: 'result/:genre',
    component: ResultComponent
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
