import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {HomePageComponent} from './components/home-page/home-page.component';
import {AppRoutingModule} from './app-routing.module';
import {PredictorComponent} from './components/predictor/predictor.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NotifierModule, NotifierOptions} from 'angular-notifier';
import { ResultComponent } from './components/result/result.component';


const notifierConfig: NotifierOptions = {
  position: {
    horizontal: {
      position: 'right'
    },
    vertical: {
      position: 'top'
    }
  }
}

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    PredictorComponent,
    NavbarComponent,
    ResultComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatProgressBarModule,
    HttpClientModule,
    NotifierModule.withConfig(notifierConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
