import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpErrorResponse, HttpEventType, HttpParams} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UploadService {
  private UPLOAD_SERVER_URL = 'https://file.io/';
  private BACKEND_URL = 'http://localhost:3000/api/v1/';
  private PREDICTOR_URL = this.BACKEND_URL + 'predictions/predict'
  constructor(
    private httpClient: HttpClient
  ) {
  }

  public upload(formData) {
    return this.httpClient.post<any>(this.UPLOAD_SERVER_URL, formData, {
      reportProgress: true,
      observe: 'events'
    });
  }

  public getPrediction(fileURLS){
    let params = new HttpParams().set('fileURLS', fileURLS);
    return this.httpClient.get(this.PREDICTOR_URL, {params : fileURLS});
  }

}
