import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpErrorResponse, HttpEventType} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class StatisticsService {
  private BACKEND_URL = 'http://localhost:3000/api/v1/';
  private STATISTICS_URL = this.BACKEND_URL + 'statistics';

  constructor(
    private httpClient: HttpClient
  ) { }

  public getStatistics(){
    return this.httpClient.get(this.STATISTICS_URL);
  }
}
