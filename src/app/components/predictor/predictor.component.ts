import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {HttpEventType, HttpErrorResponse} from '@angular/common/http';
import {of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {UploadService} from '../../services/Upload/upload.service';
import {NotifierService} from 'angular-notifier';
import {Router} from '@angular/router';


@Component({
  selector: 'app-predictor',
  templateUrl: './predictor.component.html',
  styleUrls: ['./predictor.component.css']
})
export class PredictorComponent implements OnInit {
  @ViewChild('fileUpload', {static: false}) fileUpload: ElementRef;
  files = [];
  links = [];
  uploaded = false;

  constructor(private uploadService: UploadService,
              private notifier: NotifierService,
              private router: Router) {
  }

  ngOnInit(): void {

  }


  uploadFile(file) {
    const formData = new FormData();
    formData.append('file', file.data);
    file.inProgress = true;
    this.uploadService.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        this.notifier.notify('error', 'File failed to upload, please try again!');
        return of(`${file.data.name} upload failed.`);
      })).subscribe((event: any) => {
      if (typeof (event) === 'object') {

        this.links.push(event.body.link);
        if (this.links.length == this.files.length) {
          console.log(this.links);
          this.notifier.notify('success', 'File uploaded, prediction processing');

          this.uploadService.getPrediction(this.links).subscribe(res => {

              let route = 'result/' + res['message'].toLowerCase().substr(0, res['message'].length -1);
              this.router.navigate([route]);
            },
            error => console.log(error));
        }
      }
    });
  }


  private uploadFiles() {
    this.notifier.notify('info', 'Please wait, your prediction is being processed');
    this.fileUpload.nativeElement.value = '';
    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }

  onClick() {

    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({data: file, inProgress: false, progress: 0});
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }


}
