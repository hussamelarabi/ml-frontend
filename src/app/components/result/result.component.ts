import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
genre = null;
  constructor(private route: ActivatedRoute) {
    this.genre = this.route.snapshot.paramMap.get('genre')

  }

  ngOnInit(): void {
  }


}
