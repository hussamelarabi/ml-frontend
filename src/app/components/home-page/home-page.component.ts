import {Component, OnInit} from '@angular/core';
import {StatisticsService} from '../../services/Statistics/statistics.service';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  data = null;

  constructor(
    private statisticsService: StatisticsService
  ) {
  }

  ngOnInit(): void {
    this.data = this.statisticsService.getStatistics().subscribe((res) => {
      // @ts-ignore
      this.data = res.data;
    });

  }

}
